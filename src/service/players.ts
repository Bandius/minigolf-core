import { pool } from "./mysql";
import { PlayerDb } from "../model/player/player-db";
import { PlayerDto } from "../model/player/player-dto";
import { PlayerCreateUpdate } from "../model/player/player-create-update";
import { PenaltyDb } from "../model/penalty/penalty-db";
import { ScoreDb } from "../model/score/score-db";
import { TeamDb } from "../model/team/teams-db";
import { Exist } from "../model/error/exist";
import { Table } from "../constant/table";
import { getPlayerIds } from "./scores";
import { Error } from "../model/error/error";

function getAllPlayers(): Promise<PlayerDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        "SELECT * FROM players",
        (err: any, playerDbs: PlayerDb[]) => {
          connection.release();
          if (err) return reject(err);
          const playerDtos: PlayerDto[] = playerDbs.map((playerDb) =>
            PlayerDto.fromDb(playerDb)
          );
          return resolve(playerDtos);
        }
      );
    });
  });
}

function getPlayersByEventId(eventId: number): Promise<PlayerDto[]> {
  return new Promise((resolve, reject) => {
    getPlayerIds(eventId).then((ids) => {
      if (ids.length === 0) return resolve([]);
      pool.getConnection(function (e: any, connection: any) {
        if (e) {
          connection.release();
          return reject(e);
        }
        connection.query(
          {
            sql: "SELECT * FROM players WHERE id IN (?)",
            values: [ids],
          },
          (err: any, playerDbs: PlayerDb[]) => {
            connection.release();
            if (err) return reject(err);
            const playerDtos: PlayerDto[] = playerDbs.map((playerDb) =>
              PlayerDto.fromDb(playerDb)
            );
            return resolve(playerDtos);
          }
        );
      });
    });
  });
}

function getPlayer(id: string): Promise<PlayerDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM players WHERE id=? LIMIT 0, 1",
          values: [id],
        },
        (err: any, playerDbs: PlayerDb[]) => {
          connection.release();
          if (err) return reject(err);
          const playerDb: PlayerDb = playerDbs[0];
          if (!playerDb) {
            return reject(new Error(404, "player not found by id"));
          }
          const playerDto: PlayerDto = PlayerDto.fromDb(playerDb);
          return resolve(playerDto);
        }
      );
    });
  });
}

function createPlayer(playerCreateUpdate: PlayerCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO players (firstname, lastname, category, club_id, created) VALUES (?, ?, ?, ?, CURDATE())",
          values: [
            playerCreateUpdate.firstname,
            playerCreateUpdate.lastname,
            playerCreateUpdate.category,
            playerCreateUpdate.clubId,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updatePlayer(
  id: number,
  playerCreateUpdate: PlayerCreateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE players SET firstname=?, lastname=?, category=?, club_id=? WHERE id=?",
          values: [
            playerCreateUpdate.firstname,
            playerCreateUpdate.lastname,
            playerCreateUpdate.category,
            playerCreateUpdate.clubId,
            id,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deletePlayer(id: number): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM players WHERE id=?",
          values: [id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function playerInUse(id: number): Promise<Exist[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM penalty WHERE player_id=?; SELECT * FROM scores WHERE player_id=?; SELECT * FROM teams WHERE player_1=? OR player_2=? OR player_3=? OR player_4=? OR player_5=? OR player_6=?",
          values: [id, id, id, id, id, id, id, id],
        },
        (
          err: any,
          [penalty, scores, teams]: [PenaltyDb[], ScoreDb[], TeamDb[]]
        ) => {
          connection.release();
          if (err) return reject(err);
          const exist: Exist[] = [];

          penalty.forEach((penaltyDb) => {
            exist.push(new Exist(penaltyDb.id, Table.PENALTY));
          });

          scores.forEach((scoreDb) => {
            exist.push(new Exist(scoreDb.id, Table.SCORES));
          });

          teams.forEach((teamDb) => {
            exist.push(new Exist(teamDb.id, Table.TEAMS));
          });

          return exist.length > 0 ? reject(exist) : resolve(exist);
        }
      );
    });
  });
}

export {
  getAllPlayers,
  getPlayersByEventId,
  getPlayer,
  createPlayer,
  updatePlayer,
  deletePlayer,
  playerInUse,
};
