import { pool } from "./mysql";
import { PenaltyDb } from "../model/penalty/penalty-db";
import { PenaltyDto } from "../model/penalty/penalty-dto";
import { PenaltyCreateUpdate } from "../model/penalty/penalty-create-update";
import { getLoginById } from "./login";
import { Role } from "../model/login/role";

function getAllPenalty(eventId: number): Promise<PenaltyDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        { sql: "SELECT * FROM penalty WHERE event_id=?", values: [eventId] },
        (err: any, penaltyDbs: PenaltyDb[]) => {
          connection.release();
          if (err) return reject(err);
          const penaltyDtos: PenaltyDto[] = penaltyDbs.map((penaltyDb) =>
            PenaltyDto.fromDb(penaltyDb)
          );
          return resolve(penaltyDtos);
        }
      );
    });
  });
}

function getPenalty(scoreId: number): Promise<PenaltyDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        { sql: "SELECT * FROM penalty WHERE score_id=?", values: [scoreId] },
        (err: any, penaltyDbs: PenaltyDb[]) => {
          connection.release();
          if (err) return reject(err);
          const penaltyDtos: PenaltyDto[] = penaltyDbs.map((penaltyDb) =>
            PenaltyDto.fromDb(penaltyDb)
          );
          return resolve(penaltyDtos);
        }
      );
    });
  });
}

function createPenalty(penaltyCreateUpdate: PenaltyCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO penalty (created, event_id, round_index, score_id, player_id, penalty, type, text) VALUES (CURDATE(), ?, ?, ?, ?, ?, ?, ?)",
          values: [
            penaltyCreateUpdate.eventId,
            penaltyCreateUpdate.roundIndex,
            penaltyCreateUpdate.scoreId,
            penaltyCreateUpdate.playerId,
            penaltyCreateUpdate.penalty,
            penaltyCreateUpdate.type,
            penaltyCreateUpdate.text,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updatePenalty(
  id: number,
  penaltyCreateUpdate: PenaltyCreateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE penalty SET event_id=?, round_index=?, score_id=?, player_id=?, penalty=?, type=?, text=? WHERE id=?",
          values: [
            penaltyCreateUpdate.eventId,
            penaltyCreateUpdate.roundIndex,
            penaltyCreateUpdate.scoreId,
            penaltyCreateUpdate.playerId,
            penaltyCreateUpdate.penalty,
            penaltyCreateUpdate.type,
            penaltyCreateUpdate.text,
            id,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deletePenalty(id: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM penalty WHERE id=?",
          values: [id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteAllPenaltyByEvent(eventId: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM penalty WHERE event_id=?",
          values: [eventId],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function penaltiesAuth(eventId: number, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER)
        return next();
      else if (!loginDto.eventId)
        return res.status(403).send("forbidden operation");
      else if (eventId !== loginDto.eventId)
        return res.status(403).send("forbidden operation");
      return next();
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

function penaltyAuth(penaltyId: string, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER) {
        return next();
      }

      if (!loginDto.eventId) {
        return res.status(403).send("forbidden operation");
      }

      pool.getConnection(function (e: any, connection: any) {
        if (e) {
          connection.release();
          return res.status(403).send("forbidden operation");
        }

        connection.query(
          {
            sql: "SELECT * FROM penalty WHERE id=? LIMIT 0, 1",
            values: [penaltyId],
          },
          (err: any, penaltyDbs: PenaltyDb[]) => {
            connection.release();
            if (err) return res.status(403).send("forbidden operation");
            else if (
              penaltyDbs.length === 0 ||
              penaltyDbs[0].event_id !== loginDto.eventId
            ) {
              return res.status(403).send("forbidden operation");
            }
            return next();
          }
        );
      });
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

export {
  getAllPenalty,
  getPenalty,
  createPenalty,
  updatePenalty,
  deletePenalty,
  deleteAllPenaltyByEvent,
  penaltyAuth,
  penaltiesAuth,
};
