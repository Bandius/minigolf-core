import { environment } from "../environments/environment";
import { getLoginById } from "./login";
import { Role } from "../model/login/role";

const jwt = require("jsonwebtoken");

function verifyToken(this: Role[], req: any, res: any, next: any) {
  const token =
    req.body.token || req.query.token || req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    req.body.decodedLogin = jwt.verify(token, environment.tokenKey);
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }

  if (this?.length > 0) {
    getLoginById(req.body.decodedLogin.id).then(
      (loginDto) => {
        return this.includes(loginDto.role)
          ? next()
          : res.status(403).send("forbidden operation");
      },
      (err) => {
        return res.status(403).send(err);
      }
    );
  } else {
    return next();
  }
}

export default verifyToken;
