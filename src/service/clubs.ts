import { pool } from "./mysql";
import { ClubDb } from "../model/club/club-db";
import { ClubDto } from "../model/club/club-dto";
import { ClubCreateUpdate } from "../model/club/club-create-update";
import { Exist } from "../model/error/exist";
import { Table } from "../constant/table";
import { CupDb } from "../model/cup/cup-db";
import { PlayerDb } from "../model/player/player-db";
import { CupDto } from "../model/cup/cup-dto";
import { ScoreDb } from "../model/score/score-db";
import { TeamDb } from "../model/team/teams-db";
import { Error } from "../model/error/error";

function getAllClubs(): Promise<ClubDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM clubs",
        },
        (err: any, clubDbs: ClubDb[]) => {
          connection.release();
          if (err) return reject(err);
          const clubDtos: ClubDto[] = clubDbs.map((clubDb) =>
            ClubDto.fromDb(clubDb)
          );
          return resolve(clubDtos);
        }
      );
    });
  });
}

function getClub(id: number): Promise<ClubDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }
      connection.query(
        {
          sql: "SELECT * FROM clubs WHERE id=? LIMIT 0, 1",
          values: [id],
        },
        (err: any, clubDbs: ClubDb[]) => {
          connection.release();
          if (err) return reject(err);
          const clubDb: ClubDb = clubDbs[0];
          if (clubDb) {
            const clubDto: ClubDto = ClubDto.fromDb(clubDb);
            return resolve(clubDto);
          } else {
            return reject(new Error(404, "club not found by id"));
          }
        }
      );
    });
  });
}

function createClub(clubCreateUpdate: ClubCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }
      connection.query(
        {
          sql: "INSERT INTO clubs (created, name) VALUES (CURDATE(), ?)",
          values: [clubCreateUpdate.name],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateClub(
  id: number,
  clubCreateUpdate: ClubCreateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }
      connection.query(
        {
          sql: "UPDATE clubs SET name=? WHERE id=?",
          values: [clubCreateUpdate.name, id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteClub(id: number): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }
      connection.query(
        {
          sql: "DELETE FROM clubs WHERE id=?; DELETE FROM teams WHERE club_id=?",
          values: [id, id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function clubInUse(id: number): Promise<Exist[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }
      connection.query(
        {
          sql: "SELECT * FROM cups; SELECT * FROM players WHERE club_id=?; SELECT * FROM scores WHERE club_id=?; SELECT * FROM teams WHERE club_id=?",
          values: [id, id, id],
        },
        (
          err: any,
          [cups, players, scores, teams]: [
            CupDb[],
            PlayerDb[],
            ScoreDb[],
            TeamDb[]
          ]
        ) => {
          connection.release();
          if (err) return reject(err);
          const exist: Exist[] = [];

          cups.forEach((cupDb) => {
            const cupDto: CupDto = CupDto.fromDb(cupDb);
            if (
              cupDto.clubsIdPoints?.some((clubPoint) => clubPoint.clubId === id)
            ) {
              exist.push(new Exist(cupDb.id, Table.CUPS));
            }
          });

          players.forEach((playerDb) => {
            exist.push(new Exist(playerDb.id, Table.PLAYERS));
          });

          scores.forEach((scoreDb) => {
            exist.push(new Exist(scoreDb.id, Table.SCORES));
          });

          teams.forEach((teamDb) => {
            exist.push(new Exist(teamDb.id, Table.TEAMS));
          });

          return exist.length > 0 ? reject(exist) : resolve(exist);
        }
      );
    });
  });
}

export { getAllClubs, getClub, createClub, updateClub, deleteClub, clubInUse };
