import { pool } from "./mysql";
import { TeamDto } from "../model/team/team-dto";
import { TeamDb } from "../model/team/teams-db";
import { TeamCreateUpdate } from "../model/team/team-create-update";
import { getLoginById } from "./login";
import { Role } from "../model/login/role";

function getTeams(eventId: string): Promise<TeamDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM teams WHERE event_id=?",
          values: [eventId],
        },
        (err: any, teamDbs: TeamDb[]) => {
          connection.release();
          if (err) return reject(err);
          const teamDtos: TeamDto[] = teamDbs.map((teamDb) =>
            TeamDto.fromDb(teamDb)
          );
          return resolve(teamDtos);
        }
      );
    });
  });
}

function createTeam(teamCreateUpdate: TeamCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO teams (created, event_id, club_id, player_1, player_2, player_3, player_4, player_5, player_6) VALUES (CURDATE(), ?, ?, ?, ?, ?, ?, ?, ?)",
          values: [
            teamCreateUpdate.eventId,
            teamCreateUpdate.clubId,
            teamCreateUpdate.player1,
            teamCreateUpdate.player2,
            teamCreateUpdate.player3,
            teamCreateUpdate.player4,
            teamCreateUpdate.player5,
            teamCreateUpdate.player6,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateTeam(
  id: string,
  teamCreateUpdate: TeamCreateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE teams SET event_id=?, club_id=?, player_1=?, player_2=?, player_3=?, player_4=?, player_5=?, player_6=? WHERE id=?",
          values: [
            teamCreateUpdate.eventId,
            teamCreateUpdate.clubId,
            teamCreateUpdate.player1,
            teamCreateUpdate.player2,
            teamCreateUpdate.player3,
            teamCreateUpdate.player4,
            teamCreateUpdate.player5,
            teamCreateUpdate.player6,
            id,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteTeam(id: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM teams WHERE id=?",
          values: [id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function teamsAuth(eventId: number, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER)
        return next();
      else if (!loginDto.eventId)
        return res.status(403).send("forbidden operation");
      else if (eventId !== loginDto.eventId)
        return res.status(403).send("forbidden operation");
      return next();
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

function teamAuth(teamId: string, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER) {
        return next();
      }

      if (!loginDto.eventId) {
        return res.status(403).send("forbidden operation");
      }

      pool.getConnection(function (e: any, connection: any) {
        if (e) {
          connection.release();
          return res.status(403).send("forbidden operation");
        }

        connection.query(
          {
            sql: "SELECT * FROM teams WHERE id=? LIMIT 0, 1",
            values: [teamId],
          },
          (err: any, teamDbs: TeamDb[]) => {
            connection.release();
            if (err) return res.status(403).send("forbidden operation");
            else if (
              teamDbs.length === 0 ||
              teamDbs[0].event_id !== loginDto.eventId
            ) {
              return res.status(403).send("forbidden operation");
            }
            return next();
          }
        );
      });
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

export { getTeams, createTeam, updateTeam, deleteTeam, teamsAuth, teamAuth };
