import { pool } from "./mysql";
import { LoginDto } from "../model/login/login-dto";
import { Login } from "../model/login/login";
import { LoginDb } from "../model/login/login-db";
import { environment } from "../environments/environment";
import { Error } from "../model/error/error";
import { UserCreate } from "../model/login/user-create";
import { Role } from "../model/login/role";
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

// login: admin
// password: admin123.

// login: jaro
// password: apoclyse123.

const getToken = (loginDb: LoginDb): any =>
  jwt.sign({ id: loginDb.id }, environment.tokenKey, {
    expiresIn: environment.tokenExpiresIn,
  });

function getLoginById(id: number): Promise<LoginDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM accounts WHERE id=? LIMIT 0, 1",
          values: [id],
        },
        async (err: any, loginDbs: LoginDb[]) => {
          connection.release();
          if (err) return reject(err);
          const loginDb: LoginDb = loginDbs[0];
          if (!loginDb) {
            return reject(new Error(404, "login not found by id"));
          }
          return resolve(LoginDto.fromDb(loginDb, getToken(loginDb)));
        }
      );
    });
  });
}

function getLogin(login: Login): Promise<LoginDto> {
  return new Promise((resolve, reject) => {
    if (!(login.login && login.password)) {
      return reject("login and password required");
    }

    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM accounts WHERE login=? LIMIT 0, 1",
          values: [login.login],
        },
        async (err: any, loginDbs: LoginDb[]) => {
          connection.release();
          if (err) return reject(err);
          const loginDb: LoginDb = loginDbs[0];
          if (
            loginDb &&
            (await bcrypt.compare(login.password, loginDb.password))
          ) {
            return resolve(LoginDto.fromDb(loginDb, getToken(loginDb)));
          }
          return reject("bad login");
        }
      );
    });
  });
}

function createUser(userCreate: UserCreate): Promise<any> {
  return new Promise((resolve, reject) => {
    if (!(userCreate.login && userCreate.password && userCreate.eventId)) {
      return reject("login, password and event_id required");
    }

    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM accounts WHERE login=? LIMIT 0, 1",
          values: [userCreate.login],
        },
        (err: any, loginDbs: LoginDb[]) => {
          if (err) {
            connection.release();
            return reject(err);
          } else if (loginDbs[0]) {
            connection.release();
            return reject(new Error(403, "user already exists"));
          }

          bcrypt.hash(userCreate.password, 12).then((hash: any) => {
            connection.query(
              {
                sql: "INSERT INTO accounts (created, login, password, role, event_id) VALUES (CURDATE(), ?, ?, ?, ?)",
                values: [userCreate.login, hash, Role.USER, userCreate.eventId],
              },
              (err: any, res: any) => {
                connection.release();
                if (err) return reject(err);
                return resolve(res);
              }
            );
          });
        }
      );
    });
  });
}

export { getLoginById, getLogin, createUser };
