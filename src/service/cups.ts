import { pool } from "./mysql";
import { CupCreateUpdate } from "../model/cup/cup-create-update";
import { CupDb } from "../model/cup/cup-db";
import { CupDto } from "../model/cup/cup-dto";
import { ClubPoint } from "../model/cup/club-point";
import { Error } from "../model/error/error";

function getAllCups(): Promise<CupDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM cups",
        },
        (err: any, cupDbs: CupDb[]) => {
          connection.release();
          if (err) return reject(err);
          const cupDtos: CupDto[] = cupDbs.map((cupDb) => CupDto.fromDb(cupDb));
          return resolve(cupDtos);
        }
      );
    });
  });
}

function getCup(id: string): Promise<CupDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM cups WHERE id=? LIMIT 0, 1",
          values: [id],
        },
        (err: any, cupDbs: CupDb[]) => {
          connection.release();
          if (err) return reject(err);
          const cupDb: CupDb = cupDbs[0];
          if (!cupDb) {
            return reject(new Error(404, "cup not found by id"));
          }
          const cupDto: CupDto = CupDto.fromDb(cupDb);
          return resolve(cupDto);
        }
      );
    });
  });
}

function createCup(cupCreateUpdate: CupCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO cups (created, name, text, clubs_id_points) VALUES (CURDATE(), ?, ?, ?)",
          values: [
            cupCreateUpdate.name,
            cupCreateUpdate.text,
            clubsIdPointsToString(cupCreateUpdate.clubsIdPoints),
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateCup(id: string, cupCreateUpdate: CupCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE cups SET name=?, text=?, clubs_id_points=? WHERE id=?",
          values: [
            cupCreateUpdate.name,
            cupCreateUpdate.text,
            clubsIdPointsToString(cupCreateUpdate.clubsIdPoints),
            id,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteCup(id: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM cups WHERE id=?",
          values: [id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function clubsIdPointsToString(clubPoints: ClubPoint[]): string {
  return clubPoints
    .map(
      (clubPoint) =>
        `${clubPoint.clubId}:${clubPoint.strikes}:${clubPoint.rounds}:${clubPoint.average}:${clubPoint.points}`
    )
    .join(";");
}

export { getAllCups, getCup, createCup, updateCup, deleteCup };
