import { pool } from "./mysql";
import { RoundDto } from "../model/round/round-dto";
import { RoundDb } from "../model/round/round-db";
import { RoundCreate } from "../model/round/round-create";
import { RoundUpdate } from "../model/round/round-update";
import { Error } from "../model/error/error";
import { getLoginById } from "./login";
import { Role } from "../model/login/role";

function getRound(eventId: string): Promise<RoundDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM rounds WHERE event_id=? LIMIT 0, 1",
          values: [eventId],
        },
        (err: any, roundDbs: RoundDb[]) => {
          connection.release();
          if (err) return reject(err);
          const roundDb: RoundDb = roundDbs[0];
          if (!roundDb)
            return reject(new Error(404, "round not found by eventId"));
          return resolve(RoundDto.fromDb(roundDb));
        }
      );
    });
  });
}

function createRound(roundCreate: RoundCreate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO rounds (created, event_id, rounds) VALUES (CURDATE(), ?, ?)",
          values: [roundCreate.eventId, JSON.stringify(roundCreate.rounds)],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateRound(id: string, roundUpdate: RoundUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE rounds SET rounds=? WHERE id=?",
          values: [JSON.stringify(roundUpdate.rounds), id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function roundsAuth(eventId: number, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER)
        return next();
      else if (!loginDto.eventId)
        return res.status(403).send("forbidden operation");
      else if (eventId !== loginDto.eventId)
        return res.status(403).send("forbidden operation");
      return next();
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

function roundAuth(roundId: string, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER) {
        return next();
      }

      if (!loginDto.eventId) {
        return res.status(403).send("forbidden operation");
      }

      pool.getConnection(function (e: any, connection: any) {
        if (e) {
          connection.release();
          return res.status(403).send("forbidden operation");
        }

        connection.query(
          {
            sql: "SELECT * FROM rounds WHERE id=? LIMIT 0, 1",
            values: [roundId],
          },
          (err: any, roundDbs: RoundDb[]) => {
            connection.release();
            if (err) return res.status(403).send("forbidden operation");
            else if (
              roundDbs.length === 0 ||
              roundDbs[0].event_id !== loginDto.eventId
            ) {
              return res.status(403).send("forbidden operation");
            }
            return next();
          }
        );
      });
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

export { getRound, createRound, updateRound, roundAuth, roundsAuth };
