import { environment } from "../environments/environment";

const mysql = require("mysql");
let pool: any;

const connectToMysql = (): void => {
  const config = environment.production
    ? environment.dbProd
    : environment.dbDev;

  pool = mysql.createPool({
    connectionLimit: 100,
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
    multipleStatements: true,
  });
};

export { connectToMysql, pool };
