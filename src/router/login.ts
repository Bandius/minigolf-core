import { Response } from "express";
import router from "./events";
import { Login } from "../model/login/login";
import { createUser, getLogin, getLoginById } from "../service/login";
import auth from "../service/auth";
import { LoginDto } from "../model/login/login-dto";
import { UserCreate } from "../model/login/user-create";
import { Role } from "../model/login/role";

router.get(
  "/login",
  auth,
  (req: { body: { decodedLogin: LoginDto } }, res: Response): void => {
    getLoginById(req.body.decodedLogin.id).then(
      (loginDto) => {
        res.json(loginDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post("/login", (req: { body: Login }, res: Response): void => {
  getLogin(req.body).then(
    (loginDto) => {
      res.json(loginDto);
    },
    (err) => {
      res.status(500).send(err);
    }
  );
});

router.post(
  "/user",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { body: UserCreate }, res: Response): void => {
    createUser(req.body).then(
      (loginDto) => {
        res.json(loginDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
