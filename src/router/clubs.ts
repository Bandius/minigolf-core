import express, { Request, Response, Router } from "express";
import {
  clubInUse,
  createClub,
  deleteClub,
  getAllClubs,
  getClub,
  updateClub,
} from "../service/clubs";
import { ClubCreateUpdate } from "../model/club/club-create-update";
import auth from "../service/auth";
import { Role } from "../model/login/role";

const router: Router = express.Router();

router.get("/clubs", (req: Request, res: Response): void => {
  getAllClubs().then(
    (clubDtos) => {
      res.json(clubDtos);
    },
    (err) => {
      res.status(500).send(err);
    }
  );
});

router.get(
  "/club/:id",
  (req: { params: { id: string } }, res: Response): void => {
    getClub(Number(req.params.id)).then(
      (clubDto) => {
        res.json(clubDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/clubs",
  auth,
  (req: { body: ClubCreateUpdate }, res: Response): void => {
    createClub(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/club/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (
    req: { params: { id: string }; body: ClubCreateUpdate },
    res: Response
  ): void => {
    updateClub(Number(req.params.id), req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/club/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { params: { id: string } }, res: Response): void => {
    clubInUse(Number(req.params.id))
      .then(() =>
        deleteClub(Number(req.params.id)).then(
          (_) => res.json(_),
          (err) => res.status(500).send(err)
        )
      )
      .catch((err) => res.status(403).send(err));
  }
);

export default router;
