import express, { Response, Router } from "express";
import { ScoreCreate } from "../model/score/score-create";
import {
  createScore,
  deleteScore,
  getScore,
  getScores,
  scoreAuth,
  scoresAuth,
  updateScore,
  updateScoreCategory,
} from "../service/scores";
import { ScoreUpdate } from "../model/score/score-update";
import { ScoreCategoryUpdate } from "../model/score/score-category-update";
import auth from "../service/auth";

const router: Router = express.Router();

router.get(
  "/scores/:eventId",
  (req: { params: { eventId: string } }, res: Response): void => {
    getScores(Number(req.params.eventId)).then(
      (scoreDtos) => {
        res.json(scoreDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.get(
  "/score/:id",
  (req: { params: { id: string } }, res: Response): void => {
    getScore(Number(req.params.id)).then(
      (scoreDto) => {
        res.json(scoreDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/scores",
  auth,
  (req, res, next) => scoresAuth(req.body.eventId, req, res, next),
  (req: { body: ScoreCreate }, res: Response): void => {
    createScore(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/score/:id",
  auth,
  (req, res, next) => scoreAuth(req.params.id, req, res, next),
  (req: { params: { id: string }; body: ScoreUpdate }, res: Response): void => {
    updateScore(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        console.log(err);
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/score/category/:id",
  auth,
  (req, res, next) => scoreAuth(req.params.id, req, res, next),
  (req: { params: { id: string }; body: ScoreCategoryUpdate }, res: Response): void => {
    updateScoreCategory(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        console.log(err);
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/score/:id",
  auth,
  (req, res, next) => scoreAuth(req.params.id, req, res, next),
  (req: { params: { id: string } }, res: Response): void => {
    deleteScore(req.params.id).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
