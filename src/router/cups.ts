import express, { Request, Response, Router } from "express";
import {
  createCup,
  deleteCup,
  getAllCups,
  getCup,
  updateCup,
} from "../service/cups";
import { CupCreateUpdate } from "../model/cup/cup-create-update";
import auth from "../service/auth";
import { Role } from "../model/login/role";

const router: Router = express.Router();

router.get("/cups", (req: Request, res: Response): void => {
  getAllCups().then(
    (cupDtos) => {
      res.json(cupDtos);
    },
    (err) => {
      res.status(500).send(err);
    }
  );
});

router.get(
  "/cup/:id",
  (req: { params: { id: string } }, res: Response): void => {
    getCup(req.params.id).then(
      (cupDto) => {
        res.json(cupDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/cups",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { body: CupCreateUpdate }, res: Response): void => {
    createCup(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/cup/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (
    req: { params: { id: string }; body: CupCreateUpdate },
    res: Response
  ): void => {
    updateCup(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/cup/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { params: { id: string } }, res: Response): void => {
    deleteCup(req.params.id).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
