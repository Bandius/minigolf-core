import express, { Response, Router } from "express";
import auth from "../service/auth";
import {
  createSnapshot,
  deleteSnapshot,
  getSnapshot,
} from "../service/snapshot";
import { SnapshotCreate } from "../model/snapshot/snapshot-create";

const router: Router = express.Router();

router.get(
  "/snapshot/:eventId",
  (req: { params: { eventId: string } }, res: Response): void => {
    getSnapshot(Number(req.params.eventId)).then(
      (snapshotDto) => {
        res.json(snapshotDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/snapshot",
  auth,
  (req: { body: SnapshotCreate }, res: Response): void => {
    createSnapshot(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/snapshot/:eventId",
  auth,
  (req: { params: { id: string } }, res: Response): void => {
    deleteSnapshot(req.params.id).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
