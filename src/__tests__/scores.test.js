const request = require("supertest")
let app = undefined
const sinon = require("sinon")
import * as auth from "../service/auth"

beforeEach(() => {
  sinon.stub(auth, "default")
    .callsFake(function(req, res, next){
      return next();
    });
  app = require('../app');
});

afterEach(() => {
  auth.default.restore();
})

describe("tests", () => {
  it("should return all scores in event with id 40", async () => {
    const {body, statusCode} = await (request(app).get('/api/scores/40'));

    expect(statusCode).toEqual(200);
    expect(body).toEqual(expect.arrayContaining([
      expect.objectContaining({
        id: expect.any(Number),
        created: expect.any(String),
        eventId: expect.any(Number),
        playerId: expect.any(Number),
        clubId: expect.any(Number),
        score: expect.anything(),
        playerCategory: expect.any(String)
      })
    ]))
  });

  it("should create new event", async () => {
    return (request(app).post("/api/events").send({
      name: "TEST",
      organizer: "TEST",
      dateFrom: "TESTDATE",
      dateTo: "TESTDATE"
    }).expect(200))
  });

  it("should return all courses", async () => {
    const {body, statusCode} = await (request(app).get('/api/courses'));

    expect(statusCode).toEqual(200);
    expect(body).toEqual(expect.arrayContaining([
      expect.objectContaining({
        id: expect.any(Number),
        created: expect.any(String),
        name: expect.any(String),
        lane1: expect.any(String),
        lane2: expect.any(String),
        lane3: expect.any(String),
        lane4: expect.any(String),
        lane5: expect.any(String),
        lane6: expect.any(String),
        lane7: expect.any(String),
        lane8: expect.any(String),
        lane9: expect.any(String),
        lane10: expect.any(String),
        lane11: expect.any(String),
        lane12: expect.any(String),
        lane13: expect.any(String),
        lane14: expect.any(String),
        lane15: expect.any(String),
        lane16: expect.any(String),
        lane17: expect.any(String),
        lane18: expect.any(String)
      })
    ]))
  });

  it("should create new score", async () => {
    return (request(app).post('/api/scores').send({
      id: 500,
      created: "TEST",
      eventId: 2,
      playerId: 21,
      clubId: 12,
      score: [[]],
      playerCategory: "TEST"
    }).expect(200))
  });

  it("should edit playersCatergory in scores", async () => {
    const {body, statusCode} = await (request(app).put('/api/score/category/20').send({
      playerCategory: "MS1"
    }));
    expect(statusCode).toEqual(200);
    expect(body.affectedRows).toEqual(1);
  });

  it("should create new player", async () => {
    return (request(app).post('/api/players').send({
      firstname: "TEST",
      lastname: "TEST",
      category: "TEST",
      clubId: 20
    }).expect(200));
  });

  it("should remove player from scores", async () => {
    const { statusCode } = await request(app).delete('/api/score/23');
    expect(statusCode).toEqual(200);
  });
})