export interface ClubDb {
  id: number;
  created: string;
  name: string;
}