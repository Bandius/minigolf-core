import { ClubDb } from './club-db';

export class ClubDto {
  id: number;
  created: string;
  name: string;

  constructor(id: number, created: string, name: string) {
    this.id = id;
    this.created = created;
    this.name = name;
  }

  static fromDb(clubDb: ClubDb): ClubDto {
    return new ClubDto(clubDb.id, clubDb.created, clubDb.name);
  }
}
