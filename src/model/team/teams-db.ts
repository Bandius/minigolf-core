export interface TeamDb {
  id: number;
  created: string;
  event_id: number;
  club_id: number;
  player_1: number;
  player_2: number;
  player_3: number;
  player_4: number;
  player_5: number;
  player_6: number;
}
