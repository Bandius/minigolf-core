import { TeamDb } from "./teams-db";

export class TeamDto {
  id: number;
  created: string;
  eventId: number;
  clubId: number;
  player1: number;
  player2: number;
  player3: number;
  player4: number;
  player5: number;
  player6: number;

  constructor(
    id: number,
    created: string,
    eventId: number,
    clubId: number,
    player1: number,
    player2: number,
    player3: number,
    player4: number,
    player5: number,
    player6: number
  ) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.clubId = clubId;
    this.player1 = player1;
    this.player2 = player2;
    this.player3 = player3;
    this.player4 = player4;
    this.player5 = player5;
    this.player6 = player6;
  }

  static fromDb(teamDb: TeamDb): TeamDto {
    return new TeamDto(
      teamDb.id,
      teamDb.created,
      teamDb.event_id,
      teamDb.club_id,
      teamDb.player_1,
      teamDb.player_2,
      teamDb.player_3,
      teamDb.player_4,
      teamDb.player_5,
      teamDb.player_6
    );
  }
}
