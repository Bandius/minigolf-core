export interface CourseDb {
  id: number;
  created: string;
  name: string;
  lane_1: string;
  lane_2: string;
  lane_3: string;
  lane_4: string;
  lane_5: string;
  lane_6: string;
  lane_7: string;
  lane_8: string;
  lane_9: string;
  lane_10: string;
  lane_11: string;
  lane_12: string;
  lane_13: string;
  lane_14: string;
  lane_15: string;
  lane_16: string;
  lane_17: string;
  lane_18: string;
}