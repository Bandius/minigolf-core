import { CourseDb } from "./course-db";

export class CourseDto {
  id: number;
  created: string;
  name: string;
  lane1: string;
  lane2: string;
  lane3: string;
  lane4: string;
  lane5: string;
  lane6: string;
  lane7: string;
  lane8: string;
  lane9: string;
  lane10: string;
  lane11: string;
  lane12: string;
  lane13: string;
  lane14: string;
  lane15: string;
  lane16: string;
  lane17: string;
  lane18: string;

  constructor(
    id: number,
    created: string,
    name: string,
    lane1: string,
    lane2: string,
    lane3: string,
    lane4: string,
    lane5: string,
    lane6: string,
    lane7: string,
    lane8: string,
    lane9: string,
    lane10: string,
    lane11: string,
    lane12: string,
    lane13: string,
    lane14: string,
    lane15: string,
    lane16: string,
    lane17: string,
    lane18: string
  ) {
    this.id = id;
    this.created = created;
    this.name = name;
    this.lane1 = lane1;
    this.lane2 = lane2;
    this.lane3 = lane3;
    this.lane4 = lane4;
    this.lane5 = lane5;
    this.lane6 = lane6;
    this.lane7 = lane7;
    this.lane8 = lane8;
    this.lane9 = lane9;
    this.lane10 = lane10;
    this.lane11 = lane11;
    this.lane12 = lane12;
    this.lane13 = lane13;
    this.lane14 = lane14;
    this.lane15 = lane15;
    this.lane16 = lane16;
    this.lane17 = lane17;
    this.lane18 = lane18;
  }

  static fromDb(courseDb: CourseDb): CourseDto {
    return new CourseDto(
      courseDb.id,
      courseDb.created,
      courseDb.name,
      courseDb.lane_1,
      courseDb.lane_2,
      courseDb.lane_3,
      courseDb.lane_4,
      courseDb.lane_5,
      courseDb.lane_6,
      courseDb.lane_7,
      courseDb.lane_8,
      courseDb.lane_9,
      courseDb.lane_10,
      courseDb.lane_11,
      courseDb.lane_12,
      courseDb.lane_13,
      courseDb.lane_14,
      courseDb.lane_15,
      courseDb.lane_16,
      courseDb.lane_17,
      courseDb.lane_18
    );
  }
}
