export interface UserCreate {
  login: string;
  password: string;
  eventId: number;
}
