import { Role } from "./role";

export interface LoginDb {
  id: number;
  created: string;
  login: string;
  password: string;
  role: Role;
  event_id: number;
}
