import { Round } from "./round";

export interface RoundUpdate {
  rounds: Round[];
}
