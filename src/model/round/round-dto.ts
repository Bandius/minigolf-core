import { RoundDb } from "./round-db";
import { Round } from "./round";

export class RoundDto {
  id: number;
  created: string;
  eventId: number;
  rounds: Round[];

  constructor(id: number, created: string, eventId: number, rounds: Round[]) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.rounds = rounds;
  }

  static fromDb(roundDb: RoundDb): RoundDto {
    return new RoundDto(
      roundDb.id,
      roundDb.created,
      roundDb.event_id,
      JSON.parse(roundDb.rounds)
    );
  }
}
