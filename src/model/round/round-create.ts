import { Round } from "./round";

export interface RoundCreate {
  eventId: number;
  rounds: Round[];
}
