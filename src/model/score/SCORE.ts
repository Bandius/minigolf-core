export class SCORE {
  static stringify(score: number[][]): string {
    return `${score.map((s) => s.join("")).join(";")}`;
  }

  static parse(score: string): number[][] {
    return !!score
      ? score.split(";").map((s) => s.split("").map((v) => Number(v)))
      : [];
  }
}
