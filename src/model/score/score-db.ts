export interface ScoreDb {
  id: number;
  created: string;
  event_id: number;
  player_id: number;
  club_id: number;
  score: string;
  playerCategory: string;
}
