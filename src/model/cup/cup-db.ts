export interface CupDb {
  id: number;
  created: string;
  name: string;
  text: string;
  clubs_id_points: string;
}
