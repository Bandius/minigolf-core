import { ClubPoint } from "./club-point";
import { CupDb } from "./cup-db";

export class CupDto {
  id: number;
  created: string;
  name: string;
  text: string;
  clubsIdPoints: ClubPoint[];

  constructor(
    id: number,
    created: string,
    name: string,
    text: string,
    clubsIdPoints: ClubPoint[]
  ) {
    this.id = id;
    this.created = created;
    this.name = name;
    this.text = text;
    this.clubsIdPoints = clubsIdPoints;
  }

  static fromDb(cupDb: CupDb): CupDto {
    return new CupDto(
      cupDb.id,
      cupDb.created,
      cupDb.name,
      cupDb.text,
      ClubPoint.getClubPointDbs(cupDb)
    );
  }
}
