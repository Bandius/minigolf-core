import { CupDb } from "./cup-db";

export class ClubPoint {
  clubId: number;
  strikes: number;
  rounds: number;
  average: number;
  points: number;

  constructor(
    clubId: number,
    strikes: number,
    rounds: number,
    average: number,
    points: number
  ) {
    this.clubId = clubId;
    this.strikes = strikes;
    this.rounds = rounds;
    this.average = average;
    this.points = points;
  }

  static getClubPointDbs(cupDb: CupDb): ClubPoint[] {
    return cupDb.clubs_id_points.split(";").map((clubWithPoints) => {
      const splitted = clubWithPoints.split(":");
      return new ClubPoint(
        Number(splitted[0]),
        Number(splitted[1]),
        Number(splitted[2]),
        Number(splitted[3]),
        Number(splitted[4])
      );
    });
  }
}
