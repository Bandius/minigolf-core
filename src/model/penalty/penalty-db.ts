export interface PenaltyDb {
  id: number;
  created: string;
  event_id: number;
  round_index: number;
  score_id: number;
  player_id: number;
  penalty: number;
  type: string;
  text: string;
}
