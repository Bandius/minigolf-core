import { PenaltyType } from "./penalty-type";

export interface PenaltyCreateUpdate {
  eventId: number;
  roundIndex: number;
  scoreId: number;
  playerId: number;
  penalty: number;
  type: PenaltyType;
  text: string;
}
