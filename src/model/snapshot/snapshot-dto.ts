import { SnapshotDb } from "./snapshot-db";

export class SnapshotDto {
  id: number;
  created: string;
  eventId: number;
  snapshot: string;

  constructor(id: number, created: string, eventId: number, snapshot: string) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.snapshot = snapshot;
  }

  static fromDb(snapshotDb: SnapshotDb): SnapshotDto {
    return new SnapshotDto(
      snapshotDb.id,
      snapshotDb.created,
      snapshotDb.event_id,
      snapshotDb.snapshot
    );
  }
}
