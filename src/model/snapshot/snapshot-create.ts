export interface SnapshotCreate {
  eventId: number;
  snapshot: string;
}
