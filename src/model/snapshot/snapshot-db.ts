export interface SnapshotDb {
  id: number;
  created: string;
  event_id: number;
  snapshot: string;
}
