export interface PlayerCreateUpdate {
  firstname: string;
  lastname: string;
  category: string;
  clubId: number;
}
