import { PlayerDb } from "./player-db";

export class PlayerDto {
  id: number;
  created: string;
  firstname: string;
  lastname: string;
  category: string;
  clubId: number;

  constructor(
    id: number,
    created: string,
    firstname: string,
    lastname: string,
    category: string,
    club_id: number
  ) {
    this.id = id;
    this.created = created;
    this.firstname = firstname;
    this.lastname = lastname;
    this.category = category;
    this.clubId = club_id;
  }

  static fromDb(playerDb: PlayerDb): PlayerDto {
    return new PlayerDto(
      playerDb.id,
      playerDb.created,
      playerDb.firstname,
      playerDb.lastname,
      playerDb.category,
      playerDb.club_id
    );
  }
}
