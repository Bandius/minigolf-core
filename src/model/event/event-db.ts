import { EventState } from "./event-state";

export interface EventDb {
  id: number;
  name: string;
  organizer: string;
  dateFrom: string;
  dateTo: string;
  created: string;
  state: EventState;
}
