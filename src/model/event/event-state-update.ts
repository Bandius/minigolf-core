import { EventState } from "./event-state";

export interface EventStateUpdate {
  state: EventState;
}
