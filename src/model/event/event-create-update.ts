export interface EventCreateUpdate {
  name: string;
  organizer: string;
  dateFrom: string;
  dateTo: string;
}
