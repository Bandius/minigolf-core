import { EventDb } from "./event-db";
import { EventState } from "./event-state";

export class EventDto {
  id: number;
  name: string;
  organizer: string;
  dateFrom: string;
  dateTo: string;
  created: string;
  state: EventState;

  constructor(
    id: number,
    name: string,
    organizer: string,
    dateFrom: string,
    dateTo: string,
    created: string,
    state: EventState
  ) {
    this.id = id;
    this.name = name;
    this.organizer = organizer;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.created = created;
    this.state = state;
  }

  static fromDb(eventDb: EventDb): EventDto {
    return new EventDto(
      eventDb.id,
      eventDb.name,
      eventDb.organizer,
      eventDb.dateFrom,
      eventDb.dateTo,
      eventDb.created,
      eventDb.state
    );
  }
}
