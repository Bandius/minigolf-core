module.exports = {
  "roots": [
    "<rootDir>/src"
  ],
  "preset": "ts-jest",
  "testMatch": [
    "**/__tests__/**/*.+(ts|tsx|js)",
    "**/?(*.)+(spec|test).+(ts|tsx|js)"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest",
    "^.+\\.(js|jsx)$": "babel-jest",
  },
  "moduleDirectories" : ["node_modules", "src"],
  "moduleFileExtensions": ["js", "jsx", "ts", "tsx"],
}